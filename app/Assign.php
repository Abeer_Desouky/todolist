<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assign extends Model
{

protected $fillable=['user_id','task_id'];


    public function tasks()
    {
        return $this->hasMany(Task::class);
    }
            public function users()
    {
        return $this->belongsTo(User::class);
    }
}
