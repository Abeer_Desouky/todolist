<?php

namespace App\Http\Controllers;

use App\Task;
use App\User;
use App\Assign;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $tasks=Task::all();
       
        return view('home',compact('tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
             $users=User::all();
         return view('task.create',compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $task= Task::create(request(['description']));
      

      //dd(request('users'));
      foreach( request('users') as $user_id)  
    {
       Assign::create(['user_id'=>$user_id,'task_id'=>$task->id ]);
       }
       
       // $task = new Task();
       // $task->description = $request->Desc;
       // $task->user_id = Auth::id();
       // $task->save();



        return redirect('/home');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {

           $users=User::all();
           // $assign= Assign::where('task_id', '=',$task->id)->join('users', 'assigns.user_id', '=', 'users.id')->get();
           // //$users_name= User::where('id', '=',$assign->user_id)->get();
           // //dd($assign);
           return view('task.edit',compact('task','users'));
     
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
                $task->description=$request->description;
        $task->save();
       // session()->flash('message','Skill has been Updated');
        return redirect('/home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
       
       $assign= Assign::where('task_id', '=',$task->id);
   
        $task->delete();
       $assign->delete();
        return back();
    }
}
