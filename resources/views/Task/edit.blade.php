@extends('layouts.app')

@section('content')
<section class="content">
  <div class="container">

      <div class="row justify-content-center " >
          <div class="col-md-8"> 

            {!! Form::open(['url' => 'task/'.$task->id,'method'=>'PUT']) !!}
            {{Form::token()}}

            <div class="form-group">
                {{Form::label('Task Description :')}}
                {{Form::text('description',$task->description,['class'=>'form-control' ,'required' ])}}

            </div>
            @foreach( $users as $user)
            <div class="form-group">
                @if($user->assign->task_id->seach($task->id)) 
                {{Form::checkbox('users[]',$user->id, true)}} {{$user->name}}                
                 @else
                 {{Form::checkbox('users[]',$user->id,False)}} {{$user->name}}
                @endif

            </div>

            @endforeach


            <button type="submit" class="btn btn-primary pull-right">Submit</button>

            {!! Form::close() !!}
        </div>
    </div>
</div>

</section>
@endsection
@section('footer')

@endsection
