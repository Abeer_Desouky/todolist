@extends('layouts.app')

@section('content')
    <section class="content">
        <div class="row  justify-content-center">
            <!-- left column -->
            <div class="col-md-6  col-md-offset-2">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"></h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->

                    {!! Form::open(['url' => 'task','method'=>'POST']) !!}
                    {{Form::token()}}
                    <div class="box-body">
                        <div class="form-group">
                            {{Form::label('Task Descreption :')}}
                            {{Form::text('description',null,['class'=>'form-control' ,'required' ])}}

                        </div>
                         @foreach( $users as $user)
                        <div class="form-group">
                         
                       {{Form::checkbox('users[]',$user->id)}} {{$user->name}}
                             
                        </div>
                            @endforeach
                    </div>

                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </section>
        @endsection
@section('footer')

@endsection
