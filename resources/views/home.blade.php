@extends('layouts.app')
@section('content')
    <section class="content ">

  <div class="container">
    <div class="row justify-content-center " id="main">
        <div class="col-md-8">

             <a href="/task/create" class="btn btn-primary "><i class="fa fa-edit"></i>Create Task</a>
                   

                   
                        @foreach( $tasks as $task)
                        <div class="row justify-content-center" >
        <div class="col-md-6">
            <div class="card">
                <div class="card-header "> <h5> {{$task->description}} </h5></div>

                <div class="card-body ">
                           
                                
                       
                       
                             <button onclick="alertmessage({{$task->id}});" class="btn btn-danger " data-toggle="modal" data-target="#myModal"><i class="fa fa-remove"></i> Delete</button>
                     
                       
                            <a href="task/{{$task->id}}/edit" class="btn btn-primary "><i class="fa fa-edit"></i> edit</a>

                     </div>
                 </div>
             </div>
         </div>
    <div class="row " >
        <br/>
        <br>
    </div>

                        @endforeach
                  
               
          

       </div>
          </div>
            </div>

    </section>
@endsection

@section('footer')

    <script type="text/javascript">
        function alertmessage(id){
            $('#main').append(
                `<div id="deletealert" class=" modal text-center bg-black">
  <div class="modal-dialog">
 <div class="modal-content">
                <div class="alert alert-danger">
                <h4>
                <i class="fa fa-recycle"></i>
            Are You sure Remove This Experience?
        </h4>

            </div>
            {!!Form::open(['url'=>'task/${id}','method'=>'delete'])!!}
                    <button class="btn btn-default "><i class="fa fa-thumbs-up"></i> Yes</button>
                    <a onclick='remvealert();' id='alertno' class="btn btn-default "><i class="fa fa-thumbs-down"></i> No</a>
{!! Form::close() !!}

</div></div>
                    </div>`
            );
            $('#deletealert').fadeIn('slow');
        }
        function remvealert(){
            $('#deletealert').fadeOut('slow');
            setTimeout(function () {

                $('#deletealert').remove();
            },1000)
        }
        // $(document).ready(function() {
        //     $('#edit').click(function() {

        //   // $('#save').removeAttr("hidden");
        //  $('#save').removeClass('.hidden');
        //  });
        //  });
        $(document).ready(function () {

            $('#edit').click(function () {
                $('#edit').css('display', 'none');
                $('input').removeAttr('disabled');
                $('#save').removeAttr('style');
            });


        });


    </script>
@endsection